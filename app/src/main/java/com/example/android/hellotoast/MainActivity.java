package com.example.android.hellotoast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int mCount = 0;
    private TextView mShowCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShowCount = findViewById(R.id.show_count);
    }

    public void openHelloActivity(View view) {
        Intent intent = new Intent(this, HelloActivity.class);
        intent.putExtra(getString(R.string.passed_value_key), String.valueOf(mCount));
        startActivity(intent);
    }

    public void countUp(View view) {
        mCount++;
        updateCountText();
    }

    private void updateCountText() {
        if (mShowCount != null) {
            mShowCount.setText(String.valueOf(mCount));
        }
    }
}
