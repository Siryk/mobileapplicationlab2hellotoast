package com.example.android.hellotoast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class HelloActivity extends AppCompatActivity {

    private TextView receivedTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);
        receivedTextView = findViewById(R.id.show_count_text);
        displayPassedValue();
    }

    private void displayPassedValue() {
        Intent intent = getIntent();
        if (intent != null) {
            String passedValue = intent.getStringExtra(getString(R.string.passed_value_key));
            receivedTextView.setText(passedValue);
        }
    }
}
